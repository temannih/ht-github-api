# HT-GITHUB-API

This repository contains an Android application that allows user to login via Github (two factor authentication possible) and display top Kotlin public repositories since one year ago with paging loading and offline mode support.

## Used language and libraries
- Kotlin - primary project language
- Navigation component - handles all navigation aspects, allows to avoid boilerplate code with fragments transaction, backstack etc.
- Android Architecture Components - the core of MVVM pattern
- Paging library - painless paging loading
- RxJava - efficient way to manage data chains
- Dagger - dependency injection framework
- Retrofit - to perform API call
- Glide - fluid way to load and cache images
- Room - for data persistence used in offline mode
- Reactivenetwork - allows listening to network connection state and Internet connectivity with Reactive Programming approach

## Architecture
The application is based on MVVM architecture and The project contains the following packages :

- data: contains all the data accessing and manipulating components.
- di: Dependency injection classes, mainly modules and components.
- ui: view classes and their adapters
- viewmodels: view models corresponding to each view 
- utils: Utility classes and helpers
- app : Custom Application class implementation to be adapted to injection 

## Testing strategy
- I started with the authentication part which I think is the part that needs most unit tests in the application to cover all particular cases.

- I implemented also a test for DataRepository to check if we fetch local data when user is offline.

## Estimated percentage of completion 


 90% after 16h (2 working days) starting from scratch and i need about 8 hours more to test more scenarios and identify eventual bugs and improve ui/ux  as much as possible.


## What was the most difficult part of the challenge ?

Being obliged to make compromise between perfection and deadline respect


## Things that could be improved: 

- Architecture : 
 Use more interfaces and genericity 

- UI/UX :
- Make layout more beautiful (elevation/shadows, borders raduis) -  Add a Navigation Drawer to show user info inside - Add searchview in the actionBar - Show custom message in snackBar for each specific error (actually generic message)

- Unit test : 
 Implement tests for view models and add some tests for Data Repository (if needed)




