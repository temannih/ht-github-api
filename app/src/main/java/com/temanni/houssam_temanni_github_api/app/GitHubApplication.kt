package com.temanni.houssam_temanni_github_api.app

import android.app.Application
import com.temanni.houssam_temanni_github_api.di.component.AppComponent
import com.temanni.houssam_temanni_github_api.di.component.DaggerAppComponent
import com.temanni.houssam_temanni_github_api.di.module.ApiModule
import com.temanni.houssam_temanni_github_api.di.module.AppModule


class GitHubApplication : Application() {
    @Suppress("DEPRECATION")
    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(
                AppModule(
                    applicationContext
                )
            )
            .apiModule(ApiModule())
            .build()
    }

}