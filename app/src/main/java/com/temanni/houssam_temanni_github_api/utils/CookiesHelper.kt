package com.temanni.houssam_temanni_github_api.utils

import android.content.Context
import android.os.Build
import android.webkit.CookieManager
import android.webkit.CookieSyncManager

class CookiesHelper {
    companion object {
        fun clear(context: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                CookieManager.getInstance().removeAllCookies(null)
                CookieManager.getInstance().flush()
            } else if (context != null) {
                val cookieSyncManager = CookieSyncManager.createInstance(context)
                cookieSyncManager.startSync()
                val cookieManager: CookieManager = CookieManager.getInstance()
                cookieManager.removeAllCookie()
                cookieManager.removeSessionCookie()
                cookieSyncManager.stopSync()
                cookieSyncManager.sync()
            }
        }
    }
}