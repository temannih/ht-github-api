package com.temanni.houssam_temanni_github_api.data.local

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.temanni.houssam_temanni_github_api.data.model.Repository

@Dao
interface Dao {
    @get:Query("SELECT * FROM Repository")
    val all: DataSource.Factory<Int, Repository>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: Repository)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg items: Repository)

    @Query("DELETE FROM Repository")
    fun deleteAll()
}
