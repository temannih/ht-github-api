package com.temanni.houssam_temanni_github_api.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class User(
    @SerializedName("id") val id: Long,
    @SerializedName("login") val name: String,
    @SerializedName("avatar_url") val avatarUrl: String
) : Parcelable