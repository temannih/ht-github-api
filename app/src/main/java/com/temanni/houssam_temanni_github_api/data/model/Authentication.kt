package com.temanni.houssam_temanni_github_api.data.model

import com.google.gson.annotations.SerializedName


data class Authentication(
    @SerializedName("access_token") val accessToken: String,
    @SerializedName("refresh_token") val refreshToken: String
)