package com.temanni.houssam_temanni_github_api.data.repository

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.temanni.houssam_temanni_github_api.data.model.Issue
import com.temanni.houssam_temanni_github_api.data.model.Repository
import com.temanni.houssam_temanni_github_api.data.local.GitHubDataBase
import com.temanni.houssam_temanni_github_api.data.remote.api.GitHubApi
import com.temanni.houssam_temanni_github_api.utils.ConnectivityHelper
import com.temanni.houssam_temanni_github_api.paging.source.IssuesPageDataSource
import com.temanni.houssam_temanni_github_api.paging.facotory.IssuesPageDataSourceFactory
import com.temanni.houssam_temanni_github_api.paging.source.RepositoriesPageDataSource
import com.temanni.houssam_temanni_github_api.paging.facotory.RepositoriesPageDataSourceFactory
import com.temanni.houssam_temanni_github_api.data.model.StateLiveData
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class DataRepository @Inject constructor() {
    @Inject
    lateinit var context: Context

    @Inject
    lateinit var gitHubDataBase: GitHubDataBase

    @Inject
    lateinit var connectivityHelper: ConnectivityHelper

    fun getRepositories(
        disposables: CompositeDisposable,
        state: StateLiveData,
        gitHubApi: GitHubApi
    ): LiveData<PagedList<Repository>>? {
        return if (connectivityHelper.isConnected().subscribeOn(Schedulers.io()).blockingFirst()) {
            getRemoteData(disposables, state, gitHubApi)
        } else {
            getLocalData()
        }
    }

    private fun getLocalData(): LiveData<PagedList<Repository>> {
        val factory: DataSource.Factory<Int, Repository> =
            gitHubDataBase.dao().all
        return LivePagedListBuilder(factory, RepositoriesPageDataSource.pageListConfig)
            .build()
    }

    private fun getRemoteData(
        disposables: CompositeDisposable,
        state: StateLiveData,
        gitHubApi: GitHubApi
    ): LiveData<PagedList<Repository>> {
        val sourceFactory =
            RepositoriesPageDataSourceFactory(
                disposables,
                state,
                gitHubApi,
                ::updateDataBase
            )
        return LivePagedListBuilder(sourceFactory, RepositoriesPageDataSource.pageListConfig)
            .setFetchExecutor(RepositoriesPageDataSource.executor)
            .build()
    }

    fun getIssues(
        config: PagedList.Config,
        disposables: CompositeDisposable,
        state: StateLiveData,
        gitHubApi: GitHubApi,
        currentRepository: Repository
    ): LiveData<PagedList<Issue>>? {

        val sourceFactory =
            IssuesPageDataSourceFactory(
                disposables,
                state,
                gitHubApi,
                currentRepository
            )
        return LivePagedListBuilder(sourceFactory, config)
            .setFetchExecutor(IssuesPageDataSource.executor)
            .build()

    }

    private fun updateDataBase(items: List<Repository>) {
        gitHubDataBase.dao().insertAll(*items.toTypedArray())
    }


    // Used only for Unit test
    @VisibleForTesting(otherwise = VisibleForTesting.NONE)
    fun initValues(
        connectivityHelper: ConnectivityHelper,
        gitHubDataBase: GitHubDataBase
    ) {
        this.connectivityHelper = connectivityHelper
        this.gitHubDataBase = gitHubDataBase
    }
}


