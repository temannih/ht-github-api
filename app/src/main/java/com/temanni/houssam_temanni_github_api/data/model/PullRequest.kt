package com.temanni.houssam_temanni_github_api.data.model

import com.google.gson.annotations.SerializedName

data class PullRequest(@SerializedName("html_url") val htmlUrl: String)