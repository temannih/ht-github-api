package com.temanni.houssam_temanni_github_api.data.model

import androidx.lifecycle.MutableLiveData

sealed class State

object StateInitial : State()
object StateLoadSuccess : State()
object StateProgress : State()
data class StateError(val throwable: Throwable) : State()

class StateLiveData(state: State = StateInitial) : MutableLiveData<State>() {
    init {
        value = state
    }
}