package com.temanni.houssam_temanni_github_api.data.local

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.temanni.houssam_temanni_github_api.data.model.User

class UserConverter {
    @TypeConverter
    fun fromString(value: String?): User {
        return Gson().fromJson(value, User::class.java)
    }

    @TypeConverter
    fun fromObject(user: User): String {
        val gson = Gson()
        return gson.toJson(user)
    }
}