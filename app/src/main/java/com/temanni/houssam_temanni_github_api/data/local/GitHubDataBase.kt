package com.temanni.houssam_temanni_github_api.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.temanni.houssam_temanni_github_api.data.model.Repository

@Database(entities = [Repository::class], version = 1)
@TypeConverters(
    RepositoryConverter::class,
    UserConverter::class
)
abstract class GitHubDataBase : RoomDatabase() {
    abstract fun dao(): Dao

    companion object {
        private var instance: GitHubDataBase? = null

        @Synchronized
        fun getInstance(context: Context): GitHubDataBase {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    GitHubDataBase::class.java,
                    "git_hub_data_base"
                )
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return instance as GitHubDataBase
        }
    }
}
