package com.temanni.houssam_temanni_github_api.data.model

import com.google.gson.annotations.SerializedName

data class Issue(
    @SerializedName("id") val id: Long,
    val user: User,
    @SerializedName("pull_request") var pullRequest: PullRequest?,
    val title: String,
    @SerializedName("updated_at") val updatedAt: String
)