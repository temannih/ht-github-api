package com.temanni.houssam_temanni_github_api.data.remote.api

import com.temanni.houssam_temanni_github_api.data.model.Issue
import com.temanni.houssam_temanni_github_api.data.model.Authentication
import com.temanni.houssam_temanni_github_api.data.model.User
import com.temanni.houssam_temanni_github_api.utils.CLIENT_ID
import com.temanni.houssam_temanni_github_api.utils.CLIENT_SECRET
import com.temanni.houssam_temanni_github_api.utils.REDIRECT_URI
import com.temanni.houssam_temanni_github_api.utils.TOKE_END_POINT
import com.temanni.houssam_temanni_github_api.data.model.RepositoriesResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*


interface GitHubApi {

    @Headers("Accept: application/json")
    @POST("""https://github.com/${TOKE_END_POINT}?redirect_uri=$REDIRECT_URI&client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}""")
    fun getAccessToken(@Query("code") authorizationCode: String): Single<Response<Authentication>>

    @GET("user")
    fun getAuthenticatedUser(@Header("Authorization") authHeader: String): Single<Response<User>>

    @Headers("Accept: application/json")
    @POST("https://github.com/${TOKE_END_POINT}?grant_type=refresh_token&client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}")
    fun getRefreshedToken(@Query("refresh_token") refreshToken: String): Single<Response<Authentication>>

    @GET("/search/repositories?sort=stars&order=desc&q=kotlin+language:kotlin")
    fun getRepositories(
        @Query("page") page: Int,
        @Query("per_page") pageLimit: Int
    ): Observable<Response<RepositoriesResponse>>


    @GET("/repos/{owner}/{repoName}/issues?sort=updated&direction=desc")
    fun getIssues(
        @Path("owner") owner: String,
        @Path("repoName") repoName: String,
        @Query("page") page: Int,
        @Query("per_page") pageLimit: Int,
        @Query("since") since: String
    ): Observable<Response<List<Issue>>>


}