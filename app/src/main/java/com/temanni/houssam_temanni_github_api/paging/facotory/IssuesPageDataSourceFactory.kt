package com.temanni.houssam_temanni_github_api.paging.facotory

import androidx.paging.DataSource
import com.temanni.houssam_temanni_github_api.data.model.Issue
import com.temanni.houssam_temanni_github_api.data.model.Repository
import com.temanni.houssam_temanni_github_api.data.model.StateLiveData

import com.temanni.houssam_temanni_github_api.data.remote.api.GitHubApi
import com.temanni.houssam_temanni_github_api.paging.source.IssuesPageDataSource
import io.reactivex.disposables.CompositeDisposable

class IssuesPageDataSourceFactory(
    private val disposable: CompositeDisposable,
    private val state: StateLiveData,
    private val gitHubApi: GitHubApi,
    private val currentRepository: Repository,
) : DataSource.Factory<Int, Issue>() {

    private var pageDataSource: IssuesPageDataSource? = null

    override fun create(): DataSource<Int, Issue> {
        val source =
            IssuesPageDataSource(
                disposable,
                state,
                gitHubApi,
                currentRepository
            )
        pageDataSource = source
        return source
    }
}