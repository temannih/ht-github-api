package com.temanni.houssam_temanni_github_api.di.component

import com.temanni.houssam_temanni_github_api.di.module.ApiModule
import com.temanni.houssam_temanni_github_api.di.module.AppModule
import com.temanni.houssam_temanni_github_api.ui.fragments.RepositoriesListFragment
import com.temanni.houssam_temanni_github_api.viewmodels.RepositoriesListViewModel
import com.temanni.houssam_temanni_github_api.ui.fragments.IssuesListFragment
import com.temanni.houssam_temanni_github_api.viewmodels.IssuesListViewModel
import com.temanni.houssam_temanni_github_api.ui.fragments.LoginFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ApiModule::class])
interface AppComponent {
    fun inject(injected: LoginFragment)
    fun inject(injected: RepositoriesListFragment)
    fun inject(injected: IssuesListFragment)
    fun inject(injected: RepositoriesListViewModel)
    fun inject(injected: IssuesListViewModel)

}