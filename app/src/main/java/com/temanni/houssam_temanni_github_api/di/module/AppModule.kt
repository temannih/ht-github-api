package com.temanni.houssam_temanni_github_api.di.module

import android.content.Context
import android.content.SharedPreferences
import com.temanni.houssam_temanni_github_api.R
import com.temanni.houssam_temanni_github_api.data.local.GitHubDataBase
import com.temanni.houssam_temanni_github_api.utils.ConnectivityHelper
import com.temanni.houssam_temanni_github_api.utils.SHARED_PREFERENCE_KEY
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/*
 * Module provides dependencies using application context
 */
@Module
class AppModule(private val context: Context) {

    @Singleton
    @Provides
    fun providesDataBase(): GitHubDataBase = GitHubDataBase.getInstance(context)

    @Provides
    fun providesContext(): Context = context

    @Provides
    fun providesConnectivityService(): ConnectivityHelper =
        ConnectivityHelper(context)


    @Singleton
    @Provides
    fun providesSharedPreferences(): SharedPreferences = context.getSharedPreferences(
        SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE
    )


}