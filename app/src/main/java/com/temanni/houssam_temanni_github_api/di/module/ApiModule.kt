package com.temanni.houssam_temanni_github_api.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.temanni.houssam_temanni_github_api.BuildConfig
import dagger.Module
import dagger.Provides
import com.temanni.houssam_temanni_github_api.data.remote.api.GitHubApi
import com.temanni.houssam_temanni_github_api.utils.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import com.temanni.houssam_temanni_github_api.utils.CONNECTION_TIMEOUT
import com.temanni.houssam_temanni_github_api.utils.READ_TIMEOUT
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/*
 * Module provides API dependencies
 */

@Module
class ApiModule {

    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): GitHubApi = retrofit.create(
        GitHubApi::class.java
    )

    @Provides
    fun provideRetrofit(client: OkHttpClient, gson: Gson): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideClient(interceptor: HttpLoggingInterceptor): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .connectTimeout(CONNECTION_TIMEOUT.toLong(), TimeUnit.SECONDS)
        .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
        .build()

    @Provides
    fun provideInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = if (BuildConfig.DEBUG) Level.BODY else Level.NONE
    }

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

}