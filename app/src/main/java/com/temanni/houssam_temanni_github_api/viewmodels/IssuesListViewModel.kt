package com.temanni.houssam_temanni_github_api.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.temanni.houssam_temanni_github_api.data.repository.DataRepository
import com.temanni.houssam_temanni_github_api.data.model.Issue
import com.temanni.houssam_temanni_github_api.data.model.Repository
import com.temanni.houssam_temanni_github_api.data.model.StateLiveData
import com.temanni.houssam_temanni_github_api.data.remote.api.GitHubApi
import com.temanni.houssam_temanni_github_api.paging.source.IssuesPageDataSource
import com.temanni.houssam_temanni_github_api.utils.ConnectivityHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class IssuesListViewModel : ViewModel() {

    @Inject
    lateinit var repository: DataRepository

    @Inject
    lateinit var gitHubApi: GitHubApi

    @Inject
    lateinit var connectivityHelper: ConnectivityHelper

    val state = StateLiveData()
    private val disposables = CompositeDisposable()


    fun getIssues(currentRepository: Repository): LiveData<PagedList<Issue>>? {
        return repository.getIssues(IssuesPageDataSource.pageListConfig, disposables, state, gitHubApi, currentRepository)
    }


    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}