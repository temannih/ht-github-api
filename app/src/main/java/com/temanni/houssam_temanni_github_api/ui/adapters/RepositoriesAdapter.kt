package com.temanni.houssam_temanni_github_api.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.temanni.houssam_temanni_github_api.R
import com.temanni.houssam_temanni_github_api.data.model.Repository
import com.temanni.houssam_temanni_github_api.databinding.RepositoryItemBinding
import com.temanni.houssam_temanni_github_api.viewmodels.RepositoryViewModel

class RepositoriesAdapter(
    private val onItemClick: (Repository) -> Unit
) : PagedListAdapter<Repository, RepositoriesAdapter.ViewHolder>(
    COMPARATOR
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: RepositoryItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.repository_item,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position) ?: return
        holder.bind(item, onItemClick)
    }

    class ViewHolder(private val binding: RepositoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            item: Repository,
            onItemClick: (Repository) -> Unit
        ) {
            val viewModel =
                RepositoryViewModel()
            viewModel.bind(item, onItemClick)
            binding.viewModel = viewModel
        }
    }

    companion object {
        val COMPARATOR = object : DiffUtil.ItemCallback<Repository>() {
            override fun areItemsTheSame(
                oldItem: Repository,
                newItem: Repository
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: Repository,
                newItem: Repository
            ): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}