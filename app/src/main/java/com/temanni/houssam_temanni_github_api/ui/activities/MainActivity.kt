package com.temanni.houssam_temanni_github_api.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.temanni.houssam_temanni_github_api.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}