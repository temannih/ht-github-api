package com.temanni.houssam_temanni_github_api

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.temanni.houssam_temanni_github_api.data.local.Dao
import com.temanni.houssam_temanni_github_api.data.local.GitHubDataBase
import com.temanni.houssam_temanni_github_api.data.model.StateLiveData
import com.temanni.houssam_temanni_github_api.data.remote.api.GitHubApi
import com.temanni.houssam_temanni_github_api.data.repository.DataRepository
import com.temanni.houssam_temanni_github_api.utils.ConnectivityHelper
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DataRepositoryTest {

    @MockK
    private lateinit var gitHubApi: GitHubApi

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    private lateinit var connectivityHelper: ConnectivityHelper

    @MockK
    private lateinit var gitHubDataBase: GitHubDataBase

    private lateinit var dataRepository: DataRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        dataRepository = DataRepository()
        dataRepository.initValues(connectivityHelper, gitHubDataBase)
    }

    @Test
    fun `should fetch local data base if network is not available`() {
        every { connectivityHelper.isConnected() } returns Observable.just(false)
        var dao = mockk<Dao>(relaxed = true)
        every { gitHubDataBase.dao() } returns dao
        dataRepository.getRepositories(CompositeDisposable(), StateLiveData(), gitHubApi)
        verify(exactly = 1) { dao.all }
    }

}